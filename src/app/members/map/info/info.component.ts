import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ModalController} from '@ionic/angular';
import { DeviceInfo, DeviceMarkers } from 'src/app/geo-json-template';
import { Constants } from 'src/app/constants.enum';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {

  isDeviceList = false;
  @Input() data;
  userId: number;
  url: string = Constants.ROUTE;
  deviceAllData;
  deviceMarkers: DeviceMarkers[] = [];
  lastLatLng = {};
  deviceStatus: string;
  deviceInfoValues = new DeviceInfo();

  constructor(
    private authService: AuthenticationService,
    public modalController: ModalController,
    private storage: Storage) { }

  async ngOnInit() {
    await this.storage.get(Constants.USER_ID).then(res => {
      this.userId = res;
    });
    const gMapsLink = document.getElementById('googleMapsLink');
    const anchorValue = 'https://www.google.de/maps?f=q&hl=de&q=' + this.deviceAllData.last_lat + ',' + this.deviceAllData.last_lng;
    gMapsLink.setAttribute('href', anchorValue);
    this.loadData();
  }

  async hideDevices() {
    this.isDeviceList = false;
    this.loadData();
  }

  async loadData() {
    await this.storage.get('selected-device-data').then(res => {
      const selDeviceID = res.deviceID;
      const dIndex = this.deviceMarkers.findIndex(obj =>
        obj.deviceID === selDeviceID
      );
      const index = this.data.findIndex(obj =>
        obj.id === selDeviceID
      );
      this.deviceAllData = this.data[index];
      this.loadDeviceInfo(this.deviceMarkers[dIndex]);
    });
  }

  async loadDeviceInfo(deviceInfo) {
    if (deviceInfo.lastPosition) {
      this.deviceInfoValues.battery = this.getBatteryPercentageNew(deviceInfo);
      this.deviceInfoValues.speed = deviceInfo.lastPosition.speed;
      const gMapsLink = document.getElementById('googleMapsLink');
      const anchorValue = 'https://www.google.de/maps?f=q&hl=de&q=' + deviceInfo.lastPosition.lat + ',' + deviceInfo.lastPosition.lng;
      gMapsLink.setAttribute('href', anchorValue);
      this.deviceInfoValues.time = this.convertDateUnixtoTime(new Date((deviceInfo.lastPosition.dateunix) * 1000));

      if (deviceInfo.lastPosition.dateunix) {
        if (deviceInfo.lastPosition.speed > 2) {
          this.deviceStatus = 'Moving since:';
        } else {
          this.deviceStatus = 'Stopped since:';
        }
        this.deviceInfoValues.lastLogin = this.convertDateUnixtoTime(new Date((deviceInfo.lastPosition.dateunix) * 1000));
        const date = new Date((deviceInfo.lastPosition.dateunix) * 1000);
        const days = date.getDate();
        const currentTimestampt = (+ new Date()) / 1000;
        const seconds = currentTimestampt - deviceInfo.lastPosition.dateunix;
        const measuredTime = new Date(null);
        measuredTime.setSeconds(seconds); // specify value of SECONDS
        const MHSTime = measuredTime.toISOString().substr(11, 8);
        this.deviceInfoValues.days = MHSTime + ' + ' + days.toString() + ' days';

        // tslint:disable-next-line: triple-equals
        // tslint:disable-next-line: max-line-length
        if (!(this.lastLatLng[deviceInfo.deviceID]) || (this.lastLatLng[deviceInfo.deviceID].lat !== deviceInfo.lastPosition.lat && this.lastLatLng[deviceInfo.deviceID].lng !== deviceInfo.lastPosition.lng) ) {
          await this.authService.getStreetAdrress(deviceInfo.lastPosition.lat, deviceInfo.lastPosition.lng).then(
          res => {
            if (res) {
              if (res.address) {
                this.drawStreetAddress(res);

                // save for later:
                deviceInfo.lastAddress = res;
              }
            }
          }
        ).catch (e => {
          console.log(e);
        });
        } else if (deviceInfo.lastAddress.address) {
          this.drawStreetAddress(deviceInfo.lastAddress);
        }

        this.lastLatLng[deviceInfo.deviceID] = {
          lat: deviceInfo.lastPosition.lat,
          lng: deviceInfo.lastPosition.lng
        };
      }
    } else {
      this.deviceInfoValues.battery = 'not defined';
      this.deviceInfoValues.speed = 'not defined';
      this.deviceInfoValues.time = 'not defined';
      this.deviceInfoValues.lastLogin = 'not defined';
      this.deviceInfoValues.days = 'not defined';
      this.deviceInfoValues.street = 'not defined';
      this.deviceInfoValues.building = 'not defined';
      this.deviceInfoValues.city = 'not defined';
    }
  }

  convertDateUnixtoTime(dateunix) {
    const allMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const minutes =  '0' + dateunix.getMinutes();
    const convdataTime = dateunix.getDate() + ' ' + allMonths[dateunix.getMonth()] + ' ' + dateunix.getFullYear() + ', ' +
                                dateunix.getHours() + ':' + minutes.substr(-2);
    return convdataTime;
  }

  getBatteryPercentageNew(deviceData) {
    if (!deviceData.lastPosition) {
      return '0%';
    }
    if (deviceData.modelNumber === 1010) {
      return deviceData.lastPosition.battery + '%';
    } else {
      switch (deviceData.lastPosition.battery) {
        case 6:
            return '100%';
        case 5:
            return '80-100%';
        case 4:
            return '60-80%';
        case 3:
            return '40-60%';
        case 2:
            return '20-40%';
        case 1:
            return '0-20%';
        case 0:
            return '0%';
        default:
            return '0%';
      }
    }
  }

  drawStreetAddress(streetInfo) {
    this.deviceInfoValues.street = streetInfo.address.road; // + ' ' + streetInfo.address.house_number;
    this.deviceInfoValues.building = streetInfo.address.postcode + ' ' + streetInfo.address.city + ' ' + streetInfo.address.neighbourhood;
    this.deviceInfoValues.city = streetInfo.address.country + ' (' + streetInfo.address.county + ')';
  }

  dismiss(type: string = '') {
    this.modalController.dismiss({
      type,
      dismissed: true
    });
  }
}
