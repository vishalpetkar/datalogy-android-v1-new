import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  notifications;
  constructor(private authService: AuthenticationService) { }

  async ngOnInit() {
    // this.notifications = await this.authService.getNotifications();
    await this.authService.getNotifications().then(
      res => {
        this.notifications = res.success;
      }
    ).catch (e => {
      console.log(e);
    });
    console.log(this.notifications); 
  }

}
